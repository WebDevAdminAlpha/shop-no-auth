class ApplicationController < ActionController::Base
  before_action :version

  def version
    @version = `git rev-parse HEAD`
  end
end
